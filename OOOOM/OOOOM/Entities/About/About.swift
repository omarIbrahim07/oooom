//
//  About.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/22/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import Foundation
import ObjectMapper

class About: Mappable {
    
    var content : String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        content <- map["content"]
    }
    
}
