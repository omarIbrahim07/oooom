//
//  BidOffer.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 1/23/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class BidOffer: Mappable {
    
    var id: Int?
    var price: Int?
    var createdAt: String?
    var workerID: Int?
    var workerName: String?
    var rating: Float?
    var technicianDetails: Technician?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        price <- map["price"]
        createdAt <- map["created_at"]
        workerID <- map["worker.user_id"]
        workerName <- map["worker_name"]
        rating <- map["worker.rating"]
        technicianDetails <- map["worker"]
    }
    
}
