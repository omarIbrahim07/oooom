//
//  City.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/27/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class City: Mappable {
    
    var id: Int?
    var name: String?
    var nameEn: String?
    var locationId: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        nameEn <- map["nameEN"]
        locationId <- map["location_id"]
    }
    
}
