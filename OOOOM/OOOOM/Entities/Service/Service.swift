//
//  Service.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/11/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class Service : Mappable {
    
    var id : Int?
    var name : String?
    var image : String?
    var nameEN : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        nameEN <- map["nameEN"]
        image <- map["image"]
    }
    
}
