//
//  AboutAPIManager.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/22/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import Foundation
import ObjectMapper

class AboutAPIManager: BaseAPIManager {
    
    func getAboutApp(basicDictionary params:APIParams , onSuccess: @escaping ([About])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_ABOUT_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<About>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
}
