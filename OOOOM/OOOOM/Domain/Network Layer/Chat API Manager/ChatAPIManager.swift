//
//  ChatAPIManager.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/18/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class ChatAPIManager: BaseAPIManager {
    
    func getListOfChats(basicDictionary params:APIParams , onSuccess: @escaping ([ChatList])->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: GET_LIST_OF_CHATS_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<ChatList>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    func getRoomID(basicDictionary params:APIParams , onSuccess: @escaping (RoomObject)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: GET_ROOM_ID_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let wrapper = Mapper<RoomObject>().map(JSON: response) {
                
                onSuccess(wrapper)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func sendRecord(recordData: Data?, recordName: String, basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {

        let engagementRouter = BaseRouter(method: .post, path: GET_FILE_UPLOAD_URL, parameters: params)

        super.performAudioUploadNetworkRequest(songName: recordName, songData: recordData, forRouter: engagementRouter, onSuccess: { (responseObject) in

         if let response: [String : Any] = responseObject as? [String : Any], let file: String = response["link"] as? String {
         onSuccess(file)
         }
            
         else {
             let apiError = APIError()
             onFailure(apiError)
         }
        
         }) { (apiError) in
             onFailure(apiError)
         }
    }

    func sendImage(imageData imgData : Data? ,basicDictionary params:APIParams , onSuccess: @escaping (String)->Void, onFailure: @escaping  (APIError)->Void) {

        let engagementRouter = BaseRouter(method: .post, path: GET_FILE_UPLOAD_URL, parameters: params)

        super.performUploadNetworkRequestInChat(imageData: imgData,forRouter: engagementRouter, onSuccess: { (responseObject) in

            if let response: [String : Any] = responseObject as? [String : Any], let file: String = response["link"] as? String {
//                UserDefaultManager.shared.currentUser = userWrapper

                onSuccess(file)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }

        }) { (apiError) in
            onFailure(apiError)
        }
    }

    
}
