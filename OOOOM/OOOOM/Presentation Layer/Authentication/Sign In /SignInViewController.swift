//
//  SignInViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 8/26/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class SignInViewController: BaseViewController {
    
    lazy var viewModel: SignInViewModel = {
        return SignInViewModel()
    }()
    
    var checked: Bool = false
    var error: APIError?
    var user: UserSigned?
    
    var cities: [City] = [City]()
    var choosedCity: City?
    var genders: [GenderViewModel] = [GenderViewModel]()
    var choosedGender: GenderViewModel?
    
    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var agreeOnTermsLabel: UILabel!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var havingAccountButton: UIButton!
    
    override func viewDidLoad() {
                
        super.viewDidLoad()
        configureView()
        closeKeypad()
        setIcon()
        
        // Do any additional setup after loading the view.
        initVM()
        viewModel.initFetch(locationID: 1)
        self.genders = viewModel.getGenders()
        showOptionalFieldsPopUp()
    }
    
    // MARK:- Init View Model
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.reloadCitiesClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] () in
                self?.cities = self!.viewModel.getCities()
            }
        }
        
        viewModel.updateUser = { [weak self] () in
            DispatchQueue.main.async { [weak self] () in
                self?.user = self!.viewModel.getUser()
                if let user: UserSigned = self?.user {
                    if let userId: Int = user.id {
                        self!.goToVerifyPhone(id: userId)
                    }
                }
            }
            
        }
            
        viewModel.chooseCityClosure = { [weak self] () in
            
            DispatchQueue.main.async { [weak self] in
                self?.choosedCity = self?.viewModel.choosedCity
                if let city = self?.choosedCity {
                    self?.bindCity(city: city)
                }
            }
        }
        
        viewModel.updateGender = { [weak self] () in
            DispatchQueue.main.async {
                self?.choosedGender = self!.viewModel.gender
                if let gender = self?.choosedGender {
                    self?.bindGender(gender: gender)
                }
            }
        }
        
    }
    
    func bindCity(city: City) {
        if "Lang".localized == "ar" {
            self.cityLabel.text = city.name
        } else if "Lang".localized == "en" {
            self.cityLabel.text = city.nameEn
        }
    }
    
    func bindGender(gender: GenderViewModel) {
        if "Lang".localized == "ar" {
            self.genderLabel.text = gender.typeAr
        } else if "Lang".localized == "en" {
            self.genderLabel.text = gender.type
        }

    }
    
    // MARK:- Show Time Options Picker
    func setupGendersSheet() {
        
        let sheet = UIAlertController(title: "gender alert title".localized, message: "gender alert message".localized, preferredStyle: .actionSheet)
        for gender in genders {
            if "Lang".localized == "ar" {
                sheet.addAction(UIAlertAction(title: gender.typeAr, style: .default, handler: {_ in
                    self.viewModel.getGender(gender: gender)
                }))
            } else if "Lang".localized == "en" {
                sheet.addAction(UIAlertAction(title: gender.type, style: .default, handler: {_ in
                    self.viewModel.getGender(gender: gender)
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel gender".localized, style: .cancel, handler: nil))
        
//        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)

    }

    func showOptionalFieldsPopUp() {
        let alertController = UIAlertController(title: "optional fields alert title".localized, message: "optional fields alert message".localized, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let cancelAction = UIAlertAction(title: "optional fields alert".localized, style: .cancel)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK:- Show Time Options Picker
    func setupCitiesSheet() {
        
        let sheet = UIAlertController(title: "city alert title".localized, message: "city alert message".localized, preferredStyle: .actionSheet)
        for city in cities {
            if "Lang".localized == "ar" {
                sheet.addAction(UIAlertAction(title: city.name, style: .default, handler: {_ in
                    self.viewModel.userPressed(city: city)
                }))
            } else if "Lang".localized == "en" {
                sheet.addAction(UIAlertAction(title: city.nameEn, style: .default, handler: {_ in
                    self.viewModel.userPressed(city: city)
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel city".localized, style: .cancel, handler: nil))
        
//        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)

    }
    
    func configureView() {
        
        firstNameTextField.placeholder = "first name".localized
        lastNameTextField.placeholder = "last name".localized
        phoneTextField.placeholder = "phone".localized
        emailTextField.placeholder = "email".localized
        passwordTextField.placeholder = "password".localized
        confirmPasswordTextField.placeholder = "confirm".localized
        cityLabel.text = "city".localized
        genderLabel.text = "gender".localized
        agreeOnTermsLabel.text = "terms agree".localized
        signUpButton.setTitle("sign up".localized, for: .normal)
        havingAccountButton.setTitle("having account".localized, for: .normal)
        
        
        self.navigationItem.title = "sign up".localized
        logoView.roundCorners([.bottomLeft, .bottomRight], radius: 30.0)
        bottomView.roundCorners([.topLeft, .topRight], radius: 30.0)
        signUpButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        firstNameTextField.endEditing(true)
        lastNameTextField.endEditing(true)
        phoneTextField.endEditing(true)
        emailTextField.endEditing(true)
        passwordTextField.endEditing(true)
        confirmPasswordTextField.endEditing(true)
    }
    
    func setIcon() {
        phoneTextField.setIcon(UIImage(named: "iphone-reverse")!, imageState: "Home")
        emailTextField.setIcon(UIImage(named: "new-email-envelope")!, imageState: "Home")
        firstNameTextField.setIcon(UIImage(named: "avatar")!, imageState: "Home")
        lastNameTextField.setIcon(UIImage(named: "avatar")!, imageState: "Home")
        passwordTextField.setIcon(UIImage(named: "unlocked")!, imageState: "Home")
        confirmPasswordTextField.setIcon(UIImage(named: "unlocked")!, imageState: "Home")
    }
    
//    func presentRegistration() {
//        if let SignUpNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navigationSignUpViewController") as? UINavigationController, let rootViewContoller = SignUpNavigationController.viewControllers[0] as? SignUpViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(SignUpNavigationController, animated: true, completion: nil)
//        }
//    }
    
    //MARK:- Navigation
    func presentHomeScreen(isPushNotification: Bool = false) {
        //        if let _ = UserDefaultManager.shared.currentUser, let _ = UserDefaultManager.shared.authorization {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
        appDelegate.window!.rootViewController = viewController
        appDelegate.window!.makeKeyAndVisible()
        //        }
    }
    
    func goToTermsAndConditions() {
        if let termsAndConditions = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AgreeOnTermsViewController") as? AgreeOnTermsViewController {
            //                rootViewContoller.test = "test String"
//            self.present(termsAndConditions, animated: true, completion: nil)
            navigationController?.pushViewController(termsAndConditions, animated: true)
            
            print("List Of Chats")
        }
    }
    
    func signUp() {
        
        guard let firstName: String = firstNameTextField.text, firstName.count > 0 else {
              let apiError = APIError()
              apiError.message = "برجاء إدخال الإسم الأول"
              self.viewModel.error = apiError
              self.viewModel.state = .error
              return
          }
        
        if firstName.count < 16 {
        } else {
            let apiError = APIError()
            apiError.message = "الإسم الأول يزيد عن ١٥ حرف"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let lastName: String = lastNameTextField.text, lastName.count > 0 else {
              let apiError = APIError()
              apiError.message = "برجاء إدخال الإسم الثاني"
              self.viewModel.error = apiError
              self.viewModel.state = .error
              return
          }
        
        if lastName.count < 16 {
        } else {
            let apiError = APIError()
            apiError.message = "الإسم الثاني يزيد عن ١٥ حرف"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let email: String = emailTextField.text, email.count > 0 else {
              let apiError = APIError()
              apiError.message = "برجاء إدخال البريد الإلكتروني"
              self.viewModel.error = apiError
              self.viewModel.state = .error
              return
          }
        
        guard let password: String = passwordTextField.text, password.count > 5 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال كلمة المرور أكثر من ٦ حروف"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let confirmPassword: String = confirmPasswordTextField.text, confirmPassword == password else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال تأكيد كلمة المرور بشكل صحيح"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let phone: String = phoneTextField.text, phone.count == 10 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم الجوال بشكل صحيح"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let cityID: Int = choosedCity?.id, cityID > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال المدينة"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
//        guard let genderID: Int = choosedGender?.id, genderID > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء إدخال الجنس"
//            self.viewModel.error = apiError
//            self.viewModel.state = .error
//            return
//        }
        
        guard let checked: Bool = self.checked, checked == true else {
            let apiError = APIError()
            apiError.message = "برجاء مراجعة الشروط والأحكام"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        
        self.viewModel.signUp(firstName: firstName, lastName: lastName, email: email, password: password, phone: phone, areaID: cityID, gender: choosedGender?.id ?? 0)

    }
    
    //MARK:- Navigation
    func goToVerifyPhone(id: Int) {
//        if let verifyPhoneVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerifyPhoneNavigationVC") as? VerifyPhoneViewController {
//               verifyPhoneVC.id = id
//            self.present(verifyPhoneVC, animated: true, completion: nil)
//        }
        if let verifyPhoneNavVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerifyPhoneNavigationVC") as? UINavigationController, let rootViewContoller = verifyPhoneNavVC.viewControllers[0] as? VerifyPhoneViewController {
                rootViewContoller.id = id
            self.present(verifyPhoneNavVC, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func termsAndConditionsButtonIsPressed(_ sender: Any) {
        print("Terms Button is pressed")
    }
    
    @IBAction func registrationIsPressed(_ sender: Any) {
        print("Registration is pressed")
//        presentHomeScreen()
        self.signUp()
    }
    
    @IBAction func checkIsPressed(_ sender: Any) {
        if checked == false {
            checkImage.image = UIImage(named: "check-box (1)")
            self.goToTermsAndConditions()
            checked = true
        } else if checked == true {
            checkImage.image = UIImage(named: "check-box")
            checked = false
        }
        
    }
    
    @IBAction func cityButtonIsPressed(_ sender: Any) {
        print("City Button Pressed")
        setupCitiesSheet()
    }
    
    @IBAction func genderButtonIsPressed(_ sender: Any) {
        print("Gender is pressed")
        setupGendersSheet()
    }
    

}
