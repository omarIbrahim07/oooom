//
//  VerifyPhoneViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/3/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class VerifyPhoneViewModel {
    
    var error: APIError?

    var updateLoadingStatus: (()->())?
    var updateStatus: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    // callback for interfaces
    var status: Bool? {
        didSet {
            self.updateStatus?()
        }
    }

    func initFetch(id: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "id" : id as AnyObject,
        ]
        
        AuthenticationAPIManager().sendVerificationCode(basicDictionary: params, onSuccess: { (status) in
            
            if status == "success" {
                self.state = .populated
            } else {
                self.state = .empty
            }

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func checkVerificationCode(id: Int, verificationCode: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "id" : id as AnyObject,
            "verify_code" : verificationCode as AnyObject,
        ]
        
        AuthenticationAPIManager().checkVerificationCode(basicDictionary: params, onSuccess: { (status) in
            
            self.status = status
            if status {
                self.state = .populated
            } else {
                self.state = .error
            }


        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
        
    func getError() -> APIError {
        return self.error!
    }
    
    func getStatus() -> Bool {
        return self.status!
    }
}
