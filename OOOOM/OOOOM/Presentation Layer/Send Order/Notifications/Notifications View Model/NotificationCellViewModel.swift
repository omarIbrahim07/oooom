//
//  NotificationCellViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/12/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

struct NotificationCellViewModel {
    let orderID: Int
    let message: String
    let date: String
}
