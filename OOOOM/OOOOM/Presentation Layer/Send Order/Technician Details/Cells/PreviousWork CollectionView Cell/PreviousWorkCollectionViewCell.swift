//
//  PreviousWorkCollectionViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/21/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

protocol PreviousWorkCollectionViewCellDeleteButtonDelegate {
    func didPreviousWorkImagePressed(image: String)
}

class PreviousWorkCollectionViewCell: UICollectionViewCell {
    
    var delegate: PreviousWorkCollectionViewCellDeleteButtonDelegate?

    func didPreviousWorkImagePressed(image: String) {
        if let delegateValue = delegate {
            delegateValue.didPreviousWorkImagePressed(image: image)
        }
    }

    var previousWorkCollectionCellViewModel : PreviousWorkCollectionCellViewModel? {
        didSet {
            if let image = previousWorkCollectionCellViewModel?.image {
                previousWorkImage.loadImageFromUrl(imageUrl: ImageURLWorkerWorks + image)
            }
        }
    }

    @IBOutlet weak var previousWorkImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureCell()
    }
    
    func configureCell() {
        previousWorkImage.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 0.2901960784, green: 0.6745098039, blue: 0.9137254902, alpha: 1), borderWidth: 2)
    }
    
    @IBAction func previousWorkImageButtonIsPressed(_ sender: Any) {
        if let image = previousWorkCollectionCellViewModel?.image {
            self.didPreviousWorkImagePressed(image: ImageURLWorkerWorks + image)
        }
    }

}
