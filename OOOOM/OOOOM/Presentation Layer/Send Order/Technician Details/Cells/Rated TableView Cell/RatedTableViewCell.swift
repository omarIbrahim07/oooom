//
//  RatedTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/16/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import Cosmos

class RatedTableViewCell: UITableViewCell {
    
    var rateListCellViewModel : RatedCellViewModel? {
        didSet {
            
            if let rate: Double = rateListCellViewModel?.rate {
                clientRateView.rating = rate
            }
            
            if let client: Client = rateListCellViewModel?.client {
                if let clientFirstName: String = client.firstName, let clientLastName: String = client.lastName {
                    clientNameLabel.text = clientFirstName + " " + clientLastName
                }
            }
            
            if let comment: String = rateListCellViewModel?.comment {
                clientCommentLabel.text = comment
            }
            
            if let client: Client = rateListCellViewModel?.client, let image: String = client.image {
                clientImage.loadImageFromUrl(imageUrl: ImageURL_USERS + image)
            }
            
        }
    }


    @IBOutlet weak var clientImage: UIImageView!
    @IBOutlet weak var clientNameLabel: UILabel!
    @IBOutlet weak var clientRateView: CosmosView!
    @IBOutlet weak var clientCommentLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configure()
    }
    
    func configure() {
        clientImage.layer.cornerRadius = clientImage.frame.height / 2
    }
    
}
