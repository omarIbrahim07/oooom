//
//  PreviousWorkTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/21/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

protocol previousWorkImageTableViewCellDelegate {
    func previousWorkImageButtonPressed(image: String)
}

class PreviousWorkTableViewCell: UITableViewCell {
    
    var delegate: previousWorkImageTableViewCellDelegate?
    
        //MARK:- Delegate Helpers
    func previousWorkImageButtonPressed(image: String) {
        if let delegateValue = delegate {
            delegateValue.previousWorkImageButtonPressed(image: image)
        }
    }

//    var viewModel: PreviousWorkTableViewCellViewModel = {
//        return PreviousWorkTableViewCellViewModel()
//    }()
    var viewModel: TechnicianDetailsViewModel = {
        return TechnicianDetailsViewModel()
    }()
    
    var viewModels : [PreviousWorkCollectionCellViewModel] = []{
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    enum QuantityViewItems : Int{
        case plus
        case minus
    }
    
    var didPressOnButton: ( (QuantityViewItems) -> Void )?

    
    var previousWorkImagesNumber: Int? = 10
//    var sendOrderClosure : ((Int) -> Void)? = nil
//    var chatClosure : ((Int) -> Void)? = nil


    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var reserveNowButton: UIButton!
    @IBOutlet weak var messagingButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureCell()
        registerCollectionViewCell()
        configure()


    }
    
    func configure() {
        reserveNowButton.setTitle("reserve button".localized, for: .normal)
        messagingButton.setTitle("chat button".localized, for: .normal)
    }
    
    func configureCell() {
        reserveNowButton.layer.cornerRadius = 8.0
        messagingButton.layer.cornerRadius = 8.0
        collectionView.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        reserveNowButton.tag = 0
        messagingButton.tag = 1
    }
    
    func registerCollectionViewCell() {
        collectionView.register(UINib(nibName: "PreviousWorkCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PreviousWorkCollectionViewCell")
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.reloadData()
    }
    
    @IBAction func reserveNowButtonPressed(_ sender: Any) {
        if let selectedButton = QuantityViewItems(rawValue: (sender as AnyObject).tag){
//            switch selectedButton {
//            case .plus:
//                count += 1
//            case .minus:
//                count -= 1
//            }
            didPressOnButton?(selectedButton)
        }
    }
    
}

extension PreviousWorkTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 1.5, height: 128)
    }
    

    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
//        return viewModel.numbOfCells
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return previousWorkImagesNumber!
//        print(viewModel.numbOfCells)
//        return viewModel.numbOfCells
        print(self.viewModels.count)
        return self.viewModels.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell: PreviousWorkCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PreviousWorkCollectionViewCell", for: indexPath) as? PreviousWorkCollectionViewCell {
            
            let cellVm: PreviousWorkCollectionCellViewModel = self.viewModels[indexPath.row]
            cell.previousWorkCollectionCellViewModel = cellVm
            cell.delegate = self
            return cell
        }
        return UICollectionViewCell()
    }
    
    
}

extension PreviousWorkTableViewCell: PreviousWorkCollectionViewCellDeleteButtonDelegate {
    func didPreviousWorkImagePressed(image: String) {
        previousWorkImageButtonPressed(image: image)
    }
}

