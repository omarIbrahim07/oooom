//
//  TechnicianDetailsTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/15/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import Cosmos

protocol WorkerImageTableViewCellDelegate {
    func WorkerImageButtonPressed(image: String)
}

class TechnicianDetailsTableViewCell: UITableViewCell {
    
    var delegate: WorkerImageTableViewCellDelegate?
    
        //MARK:- Delegate Helpers
    func WorkerImageButtonPressed(image: String) {
        if let delegateValue = delegate {
            delegateValue.WorkerImageButtonPressed(image: image)
        }
    }

    var photoListCellViewModel : TechnicianDetailsCellViewModel? {
        didSet {
            
            if let age = photoListCellViewModel?.age {
                ageNumberLabel.text = String(age)
            }
            
            if let duration = photoListCellViewModel?.workFrom {
                durationLabel.text = duration
            }
            
            if let workingAt = photoListCellViewModel?.workAt {
                placeLabel.text = workingAt
            }
            
            if let completedOrders = photoListCellViewModel?.completedOrders {
                numberOfCompletedOrdersLabel.text = String(completedOrders)
            }
            
            if let fees = photoListCellViewModel?.inspectionExpenses {
                feesLabel.text = String(fees)
            }
            
            if let imageURL: String = ImageURLServices + (photoListCellViewModel?.workImage ?? "") {
                technicalityImage.loadImageFromUrl(imageUrl: imageURL)
            }
            
            if let workerImageURL: String = ImageURL_USERS + (photoListCellViewModel?.image ?? "") {
                workerImage.loadImageFromUrl(imageUrl: workerImageURL)
            }
            
            if let rate = photoListCellViewModel?.rate {
                ratingView.rating = rate
            }
            
            if let workName: String = photoListCellViewModel?.workJop, let workNameEn: String = photoListCellViewModel?.workJopEn {
                if "Lang".localized == "en" {
                    workJopLabel.text = workNameEn
                } else if "Lang".localized == "ar" {
                    workJopLabel.text = workName
                }
            }
            
        }
    }

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var technicalityImage: UIImageView!
    @IBOutlet weak var workJopLabel: UILabel!
    @IBOutlet weak var workerImage: UIImageView!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var worksFromLabel: UILabel!
    @IBOutlet weak var worksAtLabel: UILabel!
    @IBOutlet weak var completedOrdersLabel: UILabel!
    @IBOutlet weak var inspectionExpensesLabel: UILabel!
    @IBOutlet weak var ageNumberLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var numberOfCompletedOrdersLabel: UILabel!
    @IBOutlet weak var feesLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
        configure()
    }
    
    func configureView() {
        cellView.layer.cornerRadius = 8.0
        workerImage.layer.cornerRadius = 8.0
    }
    
    func configure() {
        ageLabel.text = "\("age".localized)"
        worksFromLabel.text = "\("works from".localized)"
        worksAtLabel.text = "\("works at".localized)"
        completedOrdersLabel.text = "\("completed orders".localized)"
        inspectionExpensesLabel.text = "\("inspection expenses".localized)"
    }
    
    @IBAction func workerImageButtonIsPressed(_ sender: Any) {
        if let image = photoListCellViewModel?.image {
            WorkerImageButtonPressed(image: ImageURL_USERS + image)
        }
    }
}
