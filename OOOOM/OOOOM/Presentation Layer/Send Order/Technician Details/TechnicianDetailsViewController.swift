//
//  TechnicianDetailsViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/15/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class TechnicianDetailsViewController: BaseViewController {
    
    lazy var viewModel: TechnicianDetailsViewModel = {
        return TechnicianDetailsViewModel()
    }()

    var technicianName: String?
    var numberOfComments: Int? = 10
    
    var selectedTechnician: Technician?
    var selectedService: Service?
        
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setNavigationTitle()
        configureView()
        configureTableView()
        tableView.tableHeaderView?.frame.size = CGSize(width: tableView.frame.width, height: CGFloat(44))
        
        initVM()
    }
    
    func initVM() {        
            viewModel.updateLoadingStatus = { [weak self] () in
                guard let self = self else {
                    return
                }

                DispatchQueue.main.async { [weak self] in
                    guard let self = self else {
                        return
                    }
                    switch self.viewModel.state {
                    
                    case .loading:
                        self.startLoading()
                    case .populated:
                        self.stopLoadingWithSuccess()
                    case .error:
    //                    self.stopLoadingWithError(error: <#T##APIError#>)
                        self.stopLoadingWithSuccess()
                    case .empty:
                        print("")
                    }
                }
            }
        
            if let service = self.selectedService, let technician = self.selectedTechnician {
                if let serviceID: Int = service.id, let userID: Int = technician.userId, let areaID: Int = technician.areaID {
                    viewModel.initFetch(serviceID: serviceID, userID: userID, areaID: areaID, service: service)
                }
            }

            viewModel.reloadTableViewClosure = { [weak self] () in
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            }
            
        }
    
    func setNavigationTitle() {
        if let technicianName: String = self.selectedTechnician?.name {
            navigationItem.title = technicianName
        }
    }
    
    func configureView() {
        mainView.roundCorners([.bottomLeft, .bottomRight], radius: 30.0)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "TechnicianDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "TechnicianDetailsTableViewCell")
        tableView.register(UINib(nibName: "PreviousWorkTableViewCell", bundle: nil), forCellReuseIdentifier: "PreviousWorkTableViewCell")
        tableView.register(UINib(nibName: "CellView", bundle: nil), forHeaderFooterViewReuseIdentifier: CellView.reuseIdentifier)
        tableView.register(UINib(nibName: "RatedTableViewCell", bundle: nil), forCellReuseIdentifier: "RatedTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
    }
    
    // MARK:- Navigation
    
    func goToSendOrder(selectedTechnician: Technician, technicianName: String) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SendOrderViewController") as! SendOrderViewController
        
        viewController.technicianDetails = selectedTechnician
        viewController.technicianName = technicianName
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToChat(technicianID: Int, technicianName: String, selectedTechnician: Technician) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController

        viewController.technicianID = technicianID
        viewController.technicianName = technicianName
        viewController.technicianDetails = selectedTechnician
//        viewController.technicianImage = technicianImage
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToImageDetails(imageURL: String) {
        if let imageDetailsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "ImageZoomedViewController") as? ImageZoomedViewController {
            //                rootViewContoller.test = "test String"
            imageDetailsVC.imageURL = imageURL
            self.present(imageDetailsVC, animated: true, completion: nil)
            print("bidDetailsVC")
        }
    }
    
}

extension TechnicianDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
//            return 1
            return viewModel.numberOfCells
        } else if section == 1 {
            return 1
        } else if section == 2 {
            return viewModel.numberOfRatedCells
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CellView") as? CellView {
                headerView.headerLabel.text = "info".localized
                headerView.headerImage.image = UIImage(named: "info-button")
                return headerView
            }
        }
        if section == 1 {
            if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CellView") as? CellView {
                headerView.headerLabel.text = "previous works".localized
                headerView.headerImage.image = UIImage(named: "leader")
                return headerView
            }
        } else if section == 2 {
            if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CellView") as? CellView {
                headerView.headerLabel.text = "clients rate".localized
                headerView.headerImage.image = UIImage(named: "check-list (1)")
                return headerView
            }
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44  // or whatever
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            if let cell: TechnicianDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TechnicianDetailsTableViewCell", for: indexPath) as? TechnicianDetailsTableViewCell {
                cell.delegate = self
                let cellVM = viewModel.getCellViewModel(at: indexPath)
                cell.photoListCellViewModel = cellVM
                
                return cell
            }
        } else if indexPath.section == 1 {
            if let cell: PreviousWorkTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviousWorkTableViewCell", for: indexPath) as? PreviousWorkTableViewCell {
                cell.delegate = self
                cell.viewModels = viewModel.pwcellViewModels
                cell.didPressOnButton = { f in
                    if f.rawValue == 0 {
                        print("Reserve is pressed")
                        if let technicianDetails: Technician = self.selectedTechnician, let technicianName: String = self.selectedTechnician?.name {
                            self.goToSendOrder(selectedTechnician: technicianDetails, technicianName: technicianName)
                        }
                    } else {
                        print("Messaging is pressed")
                        if let technicianID: Int = self.selectedTechnician?.userId, let technicianName: String = self.selectedTechnician?.name, let selectedTechnician = self.selectedTechnician {
                            self.goToChat(technicianID: technicianID,technicianName: technicianName, selectedTechnician: selectedTechnician)
                        }
                    }
                }
                
                return cell
            }
        } else if indexPath.section == 2 {
            if let cell: RatedTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RatedTableViewCell", for: indexPath) as? RatedTableViewCell {
                
                let cellVm = viewModel.getRatedCellViewModel(at: indexPath)
                cell.rateListCellViewModel = cellVm
                
                return cell
            }
        }
        return UITableViewCell()
    }
}

extension TechnicianDetailsViewController: previousWorkImageTableViewCellDelegate {
    func previousWorkImageButtonPressed(image: String) {
            self.goToImageDetails(imageURL: image)
    }
}

extension TechnicianDetailsViewController: WorkerImageTableViewCellDelegate {
    func WorkerImageButtonPressed(image: String) {
        self.goToImageDetails(imageURL: image)
    }    
}

