//
//  TechnicianDetailsViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/14/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class TechnicianDetailsViewModel {
    
    private var technicianDetails: TechnicianDetails?
    
    var selectedTechnician: Technician?
    var workerWorks: [Work] = [Work]()
    var workerOrders: [Order] = [Order]()
    
    enum Sorting {
        case rating
        case works
        case distance
    }
    
    enum noTechs {
        case notech
        case techs
    }
    
    enum cellType {
        case details
        case previousWork
        case rating
    }
    
    var pwcellViewModels: [PreviousWorkCollectionCellViewModel] = [PreviousWorkCollectionCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var cellViewModels: TechnicianDetailsCellViewModel? {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
            
    var ratedCellViewModels: [RatedCellViewModel] = [RatedCellViewModel]() {
         didSet {
             self.reloadTableViewClosure?()
         }
     }
    
    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var numberOfRatedCells: Int {
        return ratedCellViewModels.count
    }
    
    var numberOfCells: Int {
        if self.technicianDetails != nil {
            return 1
        }
        return 0
     }

    func initFetch(serviceID: Int, userID: Int, areaID: Int,service: Service) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "service_id" : serviceID as AnyObject,
            "user_id" : userID as AnyObject,
            "area_id" : areaID as AnyObject,
        ]
        TechnicianAPIManager().getTechnicianDetails(basicDictionary: params, onSuccess: { (technicianDetails) in
            self.technicianDetails = technicianDetails
            
            if let workerWorks = self.technicianDetails?.workerWorks {
                self.workerWorks = workerWorks
            }
            
            if let workerOrders = self.technicianDetails?.workerOrders {
                self.workerOrders = workerOrders // Cache
            }
                        
//            self.processFetchedPhoto(technicians: technicians)
            print(technicianDetails)
            self.cellViewModels = self.processTechnicianDetails(technicianDetails: technicianDetails, service: service)
            self.processRatedOrders(orders: self.workerOrders)
            self.processPreviousWorks(works: self.workerWorks)
                        
            self.state = .populated

        }) { (error) in
            print(error)
        }
    }
    
    func processRatedOrders( orders: [Order] ) {
//        if let workerOrders = self.technicianDetails?.workerOrders {
//            self.workerOrders = workerOrders // Cache
//        }
        
        var vms = [RatedCellViewModel]()
        for order in orders {
            vms.append( createRatedCellViewModel(order: order) )
        }
        self.ratedCellViewModels = vms
    }
    
    func processPreviousWorks( works: [Work] ) {
        print("FEEEEEEES")
    //        if let workerOrders = self.technicianDetails?.workerOrders {
    //            self.workerOrders = workerOrders // Cache
    //        }
        var vms = [PreviousWorkCollectionCellViewModel]()
        for work in works {
            vms.append(createPreviousCellViewModel(work: work) )
        }
        self.pwcellViewModels = vms
    }
    
    func createPreviousCellViewModel( work: Work ) -> PreviousWorkCollectionCellViewModel {
        //Wrap a description
        return PreviousWorkCollectionCellViewModel(image: work.image!)
    }
    
    var numbOfCells: Int {
        return pwcellViewModels.count
     }
    
    func createRatedCellViewModel( order: Order ) -> RatedCellViewModel {

        //Wrap a description
        return RatedCellViewModel(rate: order.rating ?? 0, comment: order.comment ?? "", client: order.client!)
    }
    
    func processTechnicianDetails(technicianDetails: TechnicianDetails, service: Service) -> TechnicianDetailsCellViewModel {
        return TechnicianDetailsCellViewModel(workJop: service.name!, workJopEn: service.nameEN!, name: technicianDetails.name!, image: technicianDetails.image!, workImage: service.image!, age: technicianDetails.age!, workFrom: technicianDetails.from!, workFromEn: technicianDetails.fromEn!, workAt: technicianDetails.area!, workAtEn: technicianDetails.areaEn!, completedOrders: technicianDetails.completedOrders!, inspectionExpenses: technicianDetails.inspectionExpenses!, rate: Double(technicianDetails.rating ?? 0))
    }

    func getCellViewModel( at indexPath: IndexPath ) -> TechnicianDetailsCellViewModel {
        return cellViewModels!
    }
    
    func getRatedCellViewModel( at indexPath: IndexPath ) -> RatedCellViewModel {
        return ratedCellViewModels[indexPath.row]
    }
                        
}

