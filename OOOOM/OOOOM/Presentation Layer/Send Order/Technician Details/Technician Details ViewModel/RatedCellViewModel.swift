//
//  RatedCellViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/14/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

struct RatedCellViewModel {
    let rate: Double
    let comment: String
    let client: Client
}
