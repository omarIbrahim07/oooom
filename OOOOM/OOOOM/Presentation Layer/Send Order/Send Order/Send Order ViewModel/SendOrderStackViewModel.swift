//
//  SendOrderStackViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/25/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
struct SendOrderStackViewModel {
    let name: Bool
    let date: Bool
    let time: Bool
    let amountAgreed: Bool
}
