//
//  ClientTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/30/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class ClientTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var clientNameLabel: UILabel!
    @IBOutlet weak var clientTextMessageLAbel: UILabel!
    @IBOutlet weak var clientImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configure()
    }

    func configure() {
        containerView.roundCorners([.topRight, .bottomRight, .bottomLeft ], radius: 20)
        containerView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        clientImageView.addCornerRadius(raduis: clientImageView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }

}
