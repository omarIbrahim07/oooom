//
//  HomeViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/10/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import SideMenu
import CoreLocation
import FSPagerView

class HomeViewController: BaseViewController {
    
    lazy var viewModel: HomeViewModel = {
        return HomeViewModel()
    }()
    
    var error: APIError?
    var offers: [Offer] = [Offer]()
    
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(UINib(nibName: "Homecell", bundle: nil), forCellWithReuseIdentifier: "Homecell")
            self.pagerView.itemSize = FSPagerView.automaticSize
        }
    }
    @IBOutlet weak var pagerViewHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setNavigationTitle()
        configureSideMenu()
        configureView()
        makeCustomLogoutBarButtonItem(imageName: "Hamburger", buttonType: "left")
        makeNotificationIcon(imageName: "bell", buttonType: "right")
        configureSideMenu()
        configureCollectionView()
        
        locationManager.requestWhenInUseAuthorization()
        checkLocationManager()
        
        configureFSPageControl()
        initVM()
    }
    
    func checkLocationManager() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func initVM() {

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.collectionView.reloadData()
            }
        }
        
        viewModel.reloadOffersPagerView = { [weak self] () in
            DispatchQueue.main.async { [weak self] () in
                self?.offers = self!.viewModel.offers
//                print(self?.offerDetailsImages)
//                self?.tableView.reloadData()
                if let offers: [Offer] = self!.offers {
                    self?.pagerView.reloadData()
                    self!.pagerView.transformer = FSPagerViewTransformer(type: .cubic)
                    self!.pagerView.automaticSlidingInterval = 5.0
                }
    //                        self.pageControl.numberOfPages = 1
            }
        }

        
        viewModel.initFetch()
        
        if let id: Int = UserDefaultManager.shared.currentUser?.id {
            pagerViewHeight.constant = 50.0
            self.pagerView.isHidden = false
            viewModel.fetchOffers()
        }
    }
    
    func setNavigationTitle() {
        navigationItem.title = "home".localized
    }
    
    func configureView() {
        mainView.roundCorners([.bottomLeft, .bottomRight], radius: 30.0)
        pagerViewHeight.constant = 0.0
        self.pagerView.isHidden = true
    }
    
    func configureCollectionView() {
        collectionView.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func configureFSPageControl() {
        pagerView.delegate = self
        pagerView.dataSource = self
        pagerView.reloadData()
    }
    
    func makeCustomLogoutBarButtonItem(imageName: String, buttonType: String) {
        
        let barButton = UIButton()
        
        let containerButton = UIButton(frame: CGRect(x: -5, y: 0, width: 35, height: 35))
        
        containerButton.setImage(UIImage(named: imageName), for: .normal)
        
        containerButton.setImage(UIImage(named: imageName), for: .highlighted)
        
        containerButton.imageView?.contentMode = .scaleAspectFit
        
        if "Lang".localized == "en" {
            containerButton.addTarget(self, action: #selector(self.configureLeftSideMenu), for: UIControl.Event.touchUpInside)
        } else if "Lang".localized == "ar" {
            containerButton.addTarget(self, action: #selector(self.configureRightSideMenu), for: UIControl.Event.touchUpInside)
        }
        
        barButton.addSubview(containerButton)
        
        if buttonType == "right" {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: barButton)
        } else if buttonType == "left" {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: barButton)
        }
    }
    
    func makeNotificationIcon(imageName: String, buttonType: String) {
        
        let barButton = UIButton()
        
        let containerButton = UIButton(frame: CGRect(x: -5, y: 0, width: 35, height: 35))
        
        containerButton.setImage(UIImage(named: imageName), for: .normal)
        
        containerButton.setImage(UIImage(named: imageName), for: .highlighted)
        
        containerButton.imageView?.contentMode = .scaleAspectFit
        
        containerButton.addTarget(self, action: #selector(self.goToNotifications), for: UIControl.Event.touchUpInside)
        
        barButton.addSubview(containerButton)
        
        if buttonType == "right" {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: barButton)
        } else if buttonType == "left" {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: barButton)
        }
    }
    
    func saveLocation(longitude: Double, latitude: Double) {
        UserDefaults.standard.set(longitude, forKey: "longitude") //Bool
        UserDefaults.standard.set(latitude, forKey: "latitude") //Bool
    }
    
    @objc func configureSideMenu() {
        // Define the menus
        let leftMenuNavigationControllerr = storyboard?.instantiateViewController(withIdentifier: "LefttSideMenu") as? UISideMenuNavigationController

        SideMenuManager.default.leftMenuNavigationController?.menuWidth = view.frame.size.width - 100
        SideMenuManager.default.leftMenuNavigationController = leftMenuNavigationControllerr
        
        if let x = self.navigationController
        {
            
            SideMenuManager.default.addPanGestureToPresent(toView: x.navigationBar)
            SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: x.view, forMenu: SideMenuManager.PresentDirection.left)
        }
        
        leftMenuNavigationControllerr!.statusBarEndAlpha = 0
    }
    
    @objc func configureLeftSideMenu() {
        // Define the menus
        let menu = storyboard!.instantiateViewController(withIdentifier: "LefttSideMenu") as! UISideMenuNavigationController
        SideMenuManager.default.rightMenuNavigationController?.leftSide = true
        SideMenuManager.default.leftMenuNavigationController = menu

        present(menu, animated: true, completion: nil)
        
        // (Optional) Prevent status bar area from turning black when menu appears:
        menu.statusBarEndAlpha = 0
    }
    
    @objc func configureRightSideMenu() {
        // Define the menus
        let menu = storyboard!.instantiateViewController(withIdentifier: "LefttSideMenu") as! UISideMenuNavigationController
        SideMenuManager.default.leftMenuNavigationController?.leftSide = false
        SideMenuManager.default.rightMenuNavigationController = menu
        
        present(menu, animated: true, completion: nil)
        
        // (Optional) Prevent status bar area from turning black when menu appears:
        menu.statusBarEndAlpha = 0
    }
    
    func showCantLoginPopUp() {
        let alertController = UIAlertController(title: "go to login title".localized, message: "go to login alert message".localized, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "go to login button".localized, style: .default) { (action) in
            self.goToLogIn()
        }
        let cancelAction = UIAlertAction(title: "cancel go to login".localized, style: .cancel)
        alertController.addAction(openAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK:- Navigation
    @objc func goToNotifications() {
        guard let id: Int = UserDefaultManager.shared.currentUser?.id, id > 0 else {
            self.showCantLoginPopUp()
            return
        }
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
        //        viewController.typeIdSelected = self.typeIdSelected
        navigationController?.pushViewController(viewController, animated: true)
    }

    func goToLogIn() {
        if let signInNavigationController = storyboard?.instantiateViewController(withIdentifier: "LoginNavigationVC") as? UINavigationController, let loginViewController = signInNavigationController.viewControllers[0] as? LoginViewController {

            UIApplication.shared.keyWindow?.rootViewController = signInNavigationController
        }
    }
    
    func goToTechnicians(service: Service) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "TechniciansViewController") as! TechniciansViewController
        viewController.choosedService = service
        //        viewController.typeIdSelected = self.typeIdSelected
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToOfferDetails(offerID: Int) {
        if let offerDetailsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "OfferDetailsViewController") as? OfferDetailsViewController {
            offerDetailsVC.offerID = offerID
            //                rootViewContoller.test = "test String"
            self.present(offerDetailsVC, animated: true, completion: nil)
            print("Offer Details")
        }
    }

}

// MARK:- PagerView Delegate
extension HomeViewController: FSPagerViewDataSource,FSPagerViewDelegate {
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.offers.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "Homecell", at: index) as! Homecell
        cell.viewModels = self.offers[index]
        return cell
    }
    
    // MARK:- FSPagerView Delegate
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        
        if let offerID: Int = self.offers[index].id {
            self.goToOfferDetails(offerID: offerID)
        }
    }
    
    
}

// MARK:- CollectionView Delegate
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfCells
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell: HomeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as? HomeCollectionViewCell {
            
            let cellVM = viewModel.getCellViewModel( at: indexPath )
            cell.photoListCellViewModel = cellVM

            
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let id: Int = UserDefaultManager.shared.currentUser?.id, id > 0 else {
            self.showCantLoginPopUp()
            return
        }
        self.viewModel.userPressed(at: indexPath)
        
        if let selectedServie = self.viewModel.selectedService {
            self.goToTechnicians(service: selectedServie)
        }
        
    }
}

// MARK:- CollectionView Layout delegate
extension HomeViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.width - 40 ) / 3.0
//        return CGSize (width: width, height: width * 1.1)
        return CGSize (width: width, height: 151)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5 , left: 10, bottom: 10, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

// MARK:- Location Manager Delegate
extension HomeViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        UserDefaults.standard.set(, forKey: "ID") //Bool
        if let location = locations.first {
            print(location.coordinate)
            saveLocation(longitude: location.coordinate.longitude, latitude: location.coordinate.latitude)
        }
    }
}
