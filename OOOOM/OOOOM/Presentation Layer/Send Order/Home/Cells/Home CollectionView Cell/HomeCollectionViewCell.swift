//
//  HomeCollectionViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/13/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var serviceTitle: UILabel!
    @IBOutlet weak var serviceImage: UIImageView!
    
    var photoListCellViewModel : HomeCellViewModel? {
        didSet {
            serviceTitle.text = photoListCellViewModel?.titleText
            if let imageURL: String = ImageURLServices + photoListCellViewModel!.imageUrl {
                serviceImage.loadImageFromUrl(imageUrl: imageURL)
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
