//
//  OrderDetailsDataViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/24/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
struct OrderDetailsDataViewModel {
    let state: String
    let name: String
    let date: String
    let time: String
}
