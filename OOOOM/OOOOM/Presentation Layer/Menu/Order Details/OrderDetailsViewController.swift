//
//  OrderDetailsViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/30/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import Cosmos

class OrderDetailsViewController: BaseViewController {
        
    lazy var viewModel: OrderDetailsViewModel = {
        return OrderDetailsViewModel()
    }()
    
    var orderSent: OrderSent?
    var stack: OrderDetailsStackViewModel?
    var stack2: OrderDetailsDataViewModel?
    
    var currentDate: Date?
    
    var orderId: Int?
    var error: APIError?
    
    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var workerNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var reservationDateLabel: UILabel!
    @IBOutlet weak var amountAgreedLabel: UILabel!
    @IBOutlet weak var amountAgreedValueLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var reservationTimeLabel: UILabel!
    @IBOutlet weak var stateStackView: UIStackView!
    @IBOutlet weak var dateStackView: UIStackView!
    @IBOutlet weak var timeStackView: UIStackView!
    @IBOutlet weak var agreeRefuseStackView: UIStackView!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var refuseButton: UIButton!
    @IBOutlet weak var trackNowStackView: UIStackView!
    @IBOutlet weak var trackNowButton: UIButton!
    @IBOutlet weak var completeStackView: UIStackView!
    @IBOutlet weak var completeButton: UIButton!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var rateCosmosView: CosmosView!
    @IBOutlet weak var cancelOrderButton: UIButton!
    @IBOutlet weak var amountAgreedStackView: UIStackView!
    @IBOutlet weak var rateButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        configureView()
        configure()
        
        initVM()
        
        if let orderId = self.orderId {
            viewModel.initFetch(orderID: orderId)
        }
    }
    
    // MARK: Change String into Date
    func getDate(date: String) -> Date {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.timeZone = TimeZone.current
        let date = dateFormatter.date(from:date)!
        print(date)

        return date
    }
    
    // MARK:- Calculate differnce between two dates in hours
    func calculateDifference(from: Date, to: Date) -> Int {
        var cal = Calendar.current
        cal.timeZone = NSTimeZone.local
        let components = cal.dateComponents([.hour], from: from, to: to)
        let diff = components.hour!
        print(diff)
        return diff
    }
    
    // MARK:- Get Current Time
    func getCurrentTime() -> String {
        
        let currentDate = Date()
         
        // 1) Create a DateFormatter() object.
        let format = DateFormatter()
         
        // 2) Set the current timezone to .current
        format.timeZone = .current
         
        // 3) Set the format of the altered date.
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
         
        // 4) Set the current date, altered by timezone.
        let dateString = format.string(from: currentDate)
        print(dateString)

        return dateString
    }

    func initVM() {
        
        viewModel.updateOrderState = { [weak self] () in
            DispatchQueue.main.async {
                self?.stack = self!.viewModel.getStackViewModel()
                if let stack = self?.stack {
                    self!.bindStackView(stackView: stack)
                }
            }
        }
        
        viewModel.updateOrderDetails = { [weak self] () in
            DispatchQueue.main.async {
                self?.orderSent = self!.viewModel.orderDetails
            }
        }
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.stack2 = self!.viewModel.getDataStackViewModel()
                    if let stack2 = self?.stack2 {
                    self!.bindDataStackView(stackView: stack2)
                }
            }
        }
        
    }
    
    func bindDataStackView(stackView: OrderDetailsDataViewModel) {
        print("Bind")
        print(stackView)
        if let name: String = stackView.name {
            workerNameLabel.text = name
        }
        
        if let date: String = stackView.date {
            let correctDate = date.substring(with: 0..<10)
            reservationDateLabel.text = correctDate
        }
        
        if let time: String = stackView.time {
            let correctTime = time.substring(with: 11..<22)
            reservationTimeLabel.text = correctTime
        }
        
        if let state: String = stackView.state {
            stateLabel.text = state
        }
    }
    
    func bindStackView(stackView: OrderDetailsStackViewModel) {
        if stackView.state == true {
            stateLabel.isHidden = false
        } else {
            stateLabel.isHidden = true
        }
        
        if stackView.cancelOrder == true {
            cancelOrderButton.isHidden = false
        } else {
            cancelOrderButton.isHidden = true
        }
                        
        if stackView.rate == true {
            rateCosmosView.isHidden = false
            rateButton.isHidden = false
        } else {
            rateCosmosView.isHidden = true
            rateButton.isHidden = true
        }

        if stackView.state == true || stackView.cancelOrder == true || stackView.rate == true {
            stateStackView.isHidden = false
        } else {
            stateStackView.isHidden = true
        }
        
        if stackView.name == true {
            workerNameLabel.isHidden = false
        } else {
            workerNameLabel.isHidden = true
        }
        
        if stackView.date == true {
            reservationDateLabel.isHidden = false
        } else {
            reservationDateLabel.isHidden = true
        }
        
        if stackView.time == true {
            reservationTimeLabel.isHidden = false
        } else {
            reservationTimeLabel.isHidden = true
        }
        
        if stackView.amountAgreed == true {
            amountAgreedStackView.isHidden = false
//            amountAgreedValueLabel.isHidden = false
        } else {
            amountAgreedStackView.isHidden = true
//            amountAgreedValueLabel.isHidden = true
        }
                
        if stackView.accept == true || stackView.refuse == true {
            agreeRefuseStackView.isHidden = false
        } else {
            agreeRefuseStackView.isHidden = true
        }
        
        if stackView.trackNow == true {
            trackNowStackView.isHidden = false
        } else {
            trackNowStackView.isHidden = true
        }

        if stackView.completeOrder == true {
            completeStackView.isHidden = false
        } else {
            completeStackView.isHidden = true
        }
    }

    
    func configure() {
        nameLabel.text = "\("technician name".localized):"
        dateLabel.text = "\("date".localized):"
        timeLabel.text = "\("time".localized):"
    }
        
    func configureView() {
        acceptButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        refuseButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        trackNowButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        cancelOrderButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)

//        navView.roundCorners([.bottomLeft, .bottomRight], radius: 30.0)
//        stateView.roundCorners([.topRight, .bottomLeft], radius: 15.0)
//        effectView.roundCorners([.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 7.0)
        
        self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
        reusableView.frame = self.navView.bounds
        reusableView.titleLabel.text = "order details title".localized
        reusableView.titleImage.image = UIImage(named: "improvement")
        reusableView.backButtonDelegate = self
        self.navView.addSubview(reusableView)
    }
    
    func showNoTrackingPopUp() {
        let alertController = UIAlertController(title: "عذرًا لا يمكنك تتبُع الفني الآن !", message: "يمكنك تتبُع الفني قبل ساعة من ميعاد تتبُع الفني", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
//        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showDonePopUp() {
        let alertController = UIAlertController(title: "تم تقييم هذا الطلب من قبل", message: "الرجوع", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "إغلاق", style: .default) { (action) in
//            self.goToHomePage()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }

    
    // MARK:- Navigation
    func goToRate(orderID: Int) {
        print("Go to rate")
        if let rateVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "RateNowViewController") as? RateNowViewController {
            rateVC.orderID = orderID
            self.present(rateVC, animated: true, completion: nil)
        }
    }
    
    func goToTracking(orderID: Int, workerID: Int) {
        print("Go to Tracking")
        if let trackingVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "TrackingViewController") as? TrackingViewController {
            trackingVC.orderId = orderID
            trackingVC.workerID = workerID
            self.present(trackingVC, animated: true, completion: nil)
        }
    }
        
    // MARK:- Actions
    @IBAction func cancelButtonIsPressed(_ sender: Any) {
        print("Cancel")
        if let orderID: Int = self.orderId {
            viewModel.submitAction(orderID: orderID, orderStatusID: 7)
        }
    }
    
    @IBAction func acceptButtonIsPressed(_ sender: Any) {
        print("Accept")
        if let orderID: Int = self.orderId {
            viewModel.submitAction(orderID: orderID, orderStatusID: 4)
        }
    }
    
    @IBAction func refuseButtonIsPressed(_ sender: Any) {
        print("Refuse")
        if let orderID: Int = self.orderId {
            viewModel.submitAction(orderID: orderID, orderStatusID: 6)
        }
    }
    
    @IBAction func trackNowButtonIsPressed(_ sender: Any) {
        print("Track")
        if let orderID: Int = self.orderId, let orderStartDate: String = orderSent?.startDate, let workerID: Int = self.orderSent?.workerId {
            let currentDate: String = getCurrentTime()
            let currntDate: Date = getDate(date: currentDate)
            let orderDate: Date = getDate(date: orderStartDate)
            
            let diff = calculateDifference(from: orderDate, to: currntDate)
            
            if diff == 0 {
                goToTracking(orderID: orderID, workerID: workerID)
            } else {
                self.showNoTrackingPopUp()
            }
        }
    }
    
    @IBAction func completeButtonIsPressed(_ sender: Any) {
        print("Complete")
        if let orderID: Int = self.orderId {
            viewModel.submitAction(orderID: orderID, orderStatusID: 5)
        }
    }
    
    @IBAction func rateButtonIsPressed(_ sender: Any) {
        print("Rate")
        if let orderID: Int = self.orderId {
            if let order: OrderSent = self.orderSent {
                if let rating = order.rating {
                    self.showDonePopUp()
                } else {
                    self.goToRate(orderID: orderID)
                }
            }
        }
    }
    
}

extension OrderDetailsViewController: backButtonViewDelegate {
    
    func didbackButtonPressed() {
        print("Delegate fired")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}

