//
//  OffersViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/31/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class OffersViewController: BaseViewController {
    
    lazy var viewModel: OffersViewModel = {
        return OffersViewModel()
    }()
    
    enum ChoosedService {
        case menu
        case techs
    }
    
    var error: APIError?
    var choosedServiceEnum: ChoosedService?
    var serviceID: Int?
    var serviceNameOfTechnician: String?
    
    var window: UIWindow?
    var orderId: Int?
    
    var numberOfOffers: Int? = 10
    
    var services: [Service] = [Service]()
    var choosedService: Service?
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var latestOffersView: UIView!
    @IBOutlet weak var latestOffersButton: UIButton!
    @IBOutlet weak var specialOrdersView: UIView!
    @IBOutlet weak var specialOrdersButton: UIButton!
    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var offersCountLabel: UILabel!
    
    
    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        initVM()
    }
    
// MARK:- Init View Model
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
                        
        viewModel.reloadServicesClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] () in
                self?.services = self!.viewModel.getServices()
            }
        }
                            
        viewModel.chooseServiceClosure = { [weak self] () in
            
            DispatchQueue.main.async { [weak self] in
                self?.choosedService = self?.viewModel.choosedService
                if let service = self?.choosedService {
                    if let serviceID: Int = service.id {
                        self?.serviceID = service.id
                        self?.viewModel.fetchOffers(serviceID: serviceID, latest: 0, special: 0)
                    }
                    self?.bindService(service: service)
                }
            }
        }
        
        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                if "Lang".localized == "en" {
                    if let offersCount: Int = self!.viewModel.numberOfCells {
                        self!.offersCountLabel.text = "\(offersCount) offers available in section"
                    }
                } else if "Lang".localized == "ar" {
                    if let offersCount: Int = self!.viewModel.numberOfCells {
                        self!.offersCountLabel.text = "متاح عدد \(offersCount) عروض في قسم"
                    }
                }
            }
        }
        
        viewModel.fetchServices()
        
        switch viewModel.choosedServiceEnum {
        case .menu:
            viewModel.fetchOffers(serviceID: 1, latest: 0, special: 0)
        case .techs:
            if let serviceID: Int = self.serviceID {
                viewModel.fetchOffers(serviceID: serviceID, latest: 0, special: 0)
                if let serviceName: String = self.serviceNameOfTechnician {
                    self.serviceLabel.text = serviceName
                }
            }
        default:
            print("s")
        }
        
    }

    func bindService(service: Service) {
        if "Lang".localized == "ar" {
            self.serviceLabel.text = service.name
        } else if "Lang".localized == "en" {
            self.serviceLabel.text = service.nameEN
        }
    }
    
    func configureView() {
        self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
        reusableView.frame = self.navView.bounds
        reusableView.titleLabel.text = "offers title".localized
        reusableView.titleImage.image = UIImage(named: "offer")
        reusableView.backButtonDelegate = self
        self.navView.addSubview(reusableView)
        latestOffersView.roundCorners([.bottomLeft, .topLeft], radius: 8.0)
        specialOrdersView.roundCorners([.bottomRight, .topRight], radius: 8.0)
        specialOrdersButton.setTitle("special offers for a limited time".localized, for: .normal)
        latestOffersButton.setTitle("latest offers".localized, for: .normal)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "OfferTableViewCell", bundle: nil), forCellReuseIdentifier: "OfferTableViewCell")
        tableView.register(UINib(nibName: "SpecialOfferTableViewCell", bundle: nil), forCellReuseIdentifier: "SpecialOfferTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
// MARK:- Show Service Options Picker
    func setupServicesSheet() {
        
        let sheet = UIAlertController(title: "service alert title".localized, message: "service alert message".localized, preferredStyle: .actionSheet)
        for service in services {
            if "Lang".localized == "ar" {
                sheet.addAction(UIAlertAction(title: service.name, style: .default, handler: {_ in
                    self.viewModel.getService(service: service)
                }))
            } else if "Lang".localized == "en" {
                sheet.addAction(UIAlertAction(title: service.nameEN, style: .default, handler: {_ in
                    self.viewModel.getService(service: service)
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel service".localized, style: .cancel, handler: nil))
        
//        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)
    }
    
    // MARK:- Actions
    @IBAction func serviceButtonIsPressed(_ sender: Any) {
        print("Service Button Pressed")
        setupServicesSheet()
    }
    
    @IBAction func latestOffersButtonIsPressed(_ sender: Any) {
        if let serviceID: Int = self.serviceID {
            viewModel.fetchOffers(serviceID: serviceID, latest: 1, special: 0)
        }
    }
    
    @IBAction func specialOrdersButtonIsPressed(_ sender: Any) {
        if let serviceID: Int = self.serviceID {
            viewModel.fetchOffers(serviceID: serviceID, latest: 0, special: 1)
        }
    }
    
    
    // MARK:- Navigation
    func goToOfferDetails(offerID: Int) {
        if let offerDetailsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "OfferDetailsViewController") as? OfferDetailsViewController {
            offerDetailsVC.offerID = offerID
            //                rootViewContoller.test = "test String"
            self.present(offerDetailsVC, animated: true, completion: nil)
            print("Offer Details")
        }
    }
    
    func goToChat2(orderId: Int) {
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        if let viewController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController {
//            viewController.orderId = orderId
//            window!.rootViewController = viewController
//            window!.makeKeyAndVisible()
//        }
    }
    
    func goToChat(orderId: Int) {
        print("RRRRRRRRRRRRRRRR")
        if let editProfileVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController {
            editProfileVC.orderId = orderId
            let navigationController = UINavigationController(rootViewController: editProfileVC)
            //                rootViewContoller.test = "test String"
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        }
        
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
//        //        viewController.postID = postID
//        navigationController?.pushViewController(viewController, animated: true)

    }
}

// MARK:- UITableView Delegate
extension OffersViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return numberOfOffers ?? 0
        return viewModel.numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.viewModel.offers[indexPath.row].isSpecial == 1 {
            if let cell: SpecialOfferTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SpecialOfferTableViewCell", for: indexPath) as? SpecialOfferTableViewCell {
                                
                let cellVM = viewModel.getCellViewModel(at: indexPath)
                cell.offerCellViewModel = cellVM
                            
                return cell
            }
        } else if self.viewModel.offers[indexPath.row].isSpecial == 0 {
            if let cell: OfferTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OfferTableViewCell") as? OfferTableViewCell {
                
                let cellVM = viewModel.getCellViewModel(at: indexPath)
                cell.offerCellViewModel = cellVM
                            
                return cell
            }
        }
                
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.userPressed(at: indexPath)
        
        if let selectedOffer = self.viewModel.selectedOffer {
            if let offerID = selectedOffer.id {
                goToOfferDetails(offerID: offerID)
            }
        }
                
        tableView.deselectRow(at: indexPath, animated: true)

    }
    
    
}

// MARK:- Back Button Delegate
extension OffersViewController: backButtonViewDelegate {
    
    func didbackButtonPressed() {
        print("Delegate fired")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}
