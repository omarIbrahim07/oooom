//
//  OffersViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 2/13/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class OffersViewModel {
    
    var services: [Service]? {
        didSet {
            self.reloadServicesClosure?()
        }
    }
    
    enum ChoosedService {
        case menu
        case techs
    }
    
    var choosedServiceEnum: ChoosedService?
    
    var choosedService: Service? {
        didSet {
            self.chooseServiceClosure?()
        }
    }
    
    var offers: [Offer] = [Offer]()

    var selectedOffer: Offer? {
        didSet {
            self.updateSelectedOffer?()
        }
    }
    var error: APIError?

    enum noTechs {
        case notech
        case techs
    }

    var offerCellViewModels: [OfferCellViewModel] = [OfferCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }

    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var updateSelectedOffer: (()->())?
    var setNoTechnician: (()->())?
    var reloadServicesClosure: (()->())?
    var chooseServiceClosure: (()->())?

    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }

    var techsExistance: noTechs = .notech {
        didSet {
            self.setNoTechnician?()
        }
    }

    var numberOfCells: Int {
         return offerCellViewModels.count
     }
    
    func fetchServices() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        CitiesAPIManager().getServices(basicDictionary: params, onSuccess: { (services) in

        self.services = services // Cache
            
        self.updateFirstService()
            
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func fetchOffers(serviceID: Int, latest: Int, special: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "service_id" : serviceID as AnyObject,
            "latest" : latest as AnyObject,
            "special" : special as AnyObject,
        ]
        
        OffersAPIManager().getOffers(basicDictionary: params, onSuccess: { (offers) in

        self.processFetchedPhoto(offers: offers)
        self.state = .populated

//        self.updateFirstService()
            
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func updateFirstService() {
        switch self.choosedServiceEnum {
        case .menu:
            self.getService(service: (services?[0])!)
        case .techs:
            print("Techs")
            if let choosedService: Service = self.choosedService {
                self.getService(service: choosedService)
            }
        case .none:
            print("None")
        }

    }
    
    func getServices() -> [Service] {
        return services!
    }

    func getService(service: Service) {
        self.choosedService = service
    }

    func processFetchedPhoto( offers: [Offer] ) {
        self.offers = offers // Cache

        if self.offers.count == 0 {
            self.techsExistance = .notech
        } else {
            self.techsExistance = .techs
        }

        var vms = [OfferCellViewModel]()
        for offer in offers {
            vms.append( createCellViewModel(offer: offer) )
        }
        self.offerCellViewModels = vms
    }

    func createCellViewModel( offer: Offer ) -> OfferCellViewModel {

        return OfferCellViewModel(id: offer.id, price: offer.price, expireDate: offer.expireDate, workerID: offer.workerId, title: offer.title, description: offer.description, serviceId: offer.serviceId, firstImage: offer.firstImage, isSpecial: offer.isSpecial)
    }


    func getCellViewModel( at indexPath: IndexPath ) -> OfferCellViewModel {
        return offerCellViewModels[indexPath.row]
    }
    
    func getOffer() -> Offer {
        return selectedOffer!
    }

    func userPressed( at indexPath: IndexPath ) {
        let offer = self.offers[indexPath.row]

        self.selectedOffer = offer
    }
    
    func getError() -> APIError {
        return self.error!
    }

}
