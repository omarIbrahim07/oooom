//
//  OfferTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 2/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class OfferTableViewCell: UITableViewCell {
    
    var offerCellViewModel: OfferCellViewModel? {
        didSet {

            if let description: String = offerCellViewModel?.description {
                descriptionLabel.text = description
            }
            
            if let date: String = offerCellViewModel?.expireDate {
                var updateDate = date.substring(with: 0..<10)
                let year: String = updateDate.substring(with: 0..<4)
                let month: String = updateDate.substring(with: 5..<7)
                let day: String = updateDate.substring(with: 8..<10)
                
                if "Lang".localized == "ar" {
                    updateDate = year + "/" + month + "/" + day
                    dateLabel.text = updateDate.withArabicNumbers
                } else {
                    updateDate = day + "/" + month + "/" + year
                    dateLabel.text = updateDate
                }
            }
            
            if let image: String = offerCellViewModel?.firstImage {
                let url = URL(string: ImageURLOffers + image)
                offerImageView.kf.setImage(with: url)
            }
                        
            if "Lang".localized == "ar" {
                if let price: Int = offerCellViewModel?.price {
                    priceLabel.text = String(price) + " ريال سعودي"
                }
            } else if "Lang".localized == "en" {
                if let price: Int = offerCellViewModel?.price {
                    priceLabel.text = String(price) + " SAR"
                }
            }
        }
    }

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var offerContainerView: UIView!
    @IBOutlet weak var offerImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureCell()
        selectionStyle = .none
    }

    func configureCell() {
        offerContainerView.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        offerImageView.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
}
