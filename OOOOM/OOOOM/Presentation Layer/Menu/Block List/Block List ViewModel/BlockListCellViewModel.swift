//
//  BlockListCellViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 5/3/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
struct BlockListCellViewModel {
    let service: String
    let createdAt: String
    let id: Int
}
