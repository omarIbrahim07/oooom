//
//  BidOffersViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 1/23/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class BidOffersViewModel {

    private var bids: [BidOffer] = [BidOffer]()

    var selectedBid: BidOffer? {
        didSet {
            self.updateSelectedBid?()
        }
    }
    var error: APIError?

    enum noTechs {
        case notech
        case techs
    }

    var bidCellViewModels: [BidOfferCellViewModel] = [BidOfferCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }

    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var updateSelectedBid: (()->())?
    var setNoTechnician: (()->())?

    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }

    var techsExistance: noTechs = .notech {
        didSet {
            self.setNoTechnician?()
        }
    }

    var numberOfCells: Int {
         return bidCellViewModels.count
     }

    func initFetch(bidID: Int) {
        state = .loading

        let params: [String : AnyObject] = [
            "bid_id" : bidID as AnyObject
        ]

        TechnicianAPIManager().getBidOffers(basicDictionary: params, onSuccess: { (bids) in

            self.processFetchedPhoto(bids: bids)
            self.state = .populated

        }) { (error) in
            print(error)
        }
    }

    func processFetchedPhoto( bids: [BidOffer] ) {
        self.bids = bids // Cache

        if self.bids.count == 0 {
            self.techsExistance = .notech
        } else {
            self.techsExistance = .techs
        }

        var vms = [BidOfferCellViewModel]()
        for bid in bids {
            vms.append( createCellViewModel(bid: bid) )
        }
        self.bidCellViewModels = vms
    }

    func createCellViewModel( bid: BidOffer ) -> BidOfferCellViewModel {

        return BidOfferCellViewModel(id: bid.id, price: bid.price, createdAt: bid.createdAt, workerID: bid.workerID, workerName: bid.workerName, rating: bid.rating, technicianDetails: bid.technicianDetails)
    }


    func getCellViewModel( at indexPath: IndexPath ) -> BidOfferCellViewModel {
        return bidCellViewModels[indexPath.row]
    }
    
    func getBidOffer() -> BidOffer {
        return selectedBid!
    }

    func userPressed( at indexPath: IndexPath ) {
        let bid = self.bids[indexPath.row]

        self.selectedBid = bid
    }
    
    func getError() -> APIError {
        return self.error!
    }

}
