//
//  BidOfferCellViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 1/23/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct BidOfferCellViewModel {
    var id: Int?
    var price: Int?
    var createdAt: String?
    var workerID: Int?
    var workerName: String?
    var rating: Float?
    var technicianDetails: Technician?
}
