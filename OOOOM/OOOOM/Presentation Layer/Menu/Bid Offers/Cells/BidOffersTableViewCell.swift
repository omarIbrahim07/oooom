//
//  OffersTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/31/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import Cosmos

class BidOffersTableViewCell: UITableViewCell {
        
    var bidCellViewModel: BidOfferCellViewModel? {
        didSet {
            if let workerName: String = bidCellViewModel?.workerName {
                workerNameLabel.text = workerName
            }
            
            if let date: String = bidCellViewModel?.createdAt {
                dateLabel.text = date
            }
            
            if let rating: Float = bidCellViewModel?.rating {
                workerRate.rating = Double(rating)
            }
            
            if "Lang".localized == "ar" {
                if let price: Int = bidCellViewModel?.price {
                    priceLabel.text = String(price) + "ريال سعودي"
                }
            } else if "Lang".localized == "en" {
                if let price: Int = bidCellViewModel?.price {
                    priceLabel.text = String(price) + " SAR"
                }
            }
        }
    }

    var didPressOnButton: ( (Int) -> Void )?
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var workerNameLabel: UILabel!
    @IBOutlet weak var workerRate: CosmosView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var ChatButton: UIButton!
    @IBOutlet weak var chatImageView: UIImageView!
    @IBOutlet weak var chatLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
        configure()
    }
    
    func configure() {
        chatLabel.text = "chat button".localized
    }
    
    func configureView() {
        containerView.layer.cornerRadius = 8.0
        chatView.layer.cornerRadius = 8.0
        chatImageView.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }

    @IBAction func ChatButtonPressed(_ sender: Any) {
        
        print(ChatButton.tag)
        didPressOnButton?(ChatButton.tag)
    }

    
}
