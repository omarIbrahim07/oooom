//
//  BidOffersViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 1/22/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class BidOffersViewController: BaseViewController {

    lazy var viewModel: BidOffersViewModel = {
        return BidOffersViewModel()
    }()

    var window: UIWindow?
    var orderId: Int?
    var error: APIError?
    
    var selectedBid: BidOffer?
    
    var numberOfOffers: Int? = 10
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        
        initVM()
    }
    
    func configureView() {
        self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
        reusableView.frame = self.navView.bounds
        reusableView.titleLabel.text = "bid offers title".localized
        reusableView.titleImage.image = UIImage(named: "offer")
        reusableView.backButtonDelegate = self
        self.navView.addSubview(reusableView)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "BidOffersTableViewCell", bundle: nil), forCellReuseIdentifier: "BidOffersTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
    }
    
    func initVM() {

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    self.stopLoadingWithSuccess()
                }
            }
        }

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.updateSelectedBid = { [weak self] () in
            DispatchQueue.main.async {
                self?.selectedBid = self!.viewModel.getBidOffer()
                if let bid = self?.selectedBid {
                    print(bid)
                    self?.goToChat(orderId: bid.id!, workerID: bid.workerID!, workerName: bid.workerName!, price: bid.price!, technicianDetails: bid.technicianDetails!)
                }
            }
        }
        
        if let bidID: Int = self.orderId {
            viewModel.initFetch(bidID: bidID)
        }
    }

    
    func goToChat2(orderId: Int) {
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        if let viewController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController {
//            viewController.orderId = orderId
//            window!.rootViewController = viewController
//            window!.makeKeyAndVisible()
//        }
    }
        
    func goToChat(orderId: Int, workerID: Int, workerName: String, price: Int, technicianDetails: Technician) {
        print("RRRRRRRRRRRRRRRR")
        if let editProfileVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController {
            
            editProfileVC.orderId = orderId
            editProfileVC.technicianName = workerName
            editProfileVC.technicianID = workerID
            editProfileVC.bidPrice = price
            editProfileVC.isBid = true
            editProfileVC.technicianDetails = technicianDetails
            editProfileVC.technicianDetails?.name = workerName
            
            let navigationController = UINavigationController(rootViewController: editProfileVC)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        }
        
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
//        //        viewController.postID = postID
//        navigationController?.pushViewController(viewController, animated: true)

    }
}

extension BidOffersViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCells 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: BidOffersTableViewCell = tableView.dequeueReusableCell(withIdentifier: "BidOffersTableViewCell", for: indexPath) as? BidOffersTableViewCell {
            
            cell.ChatButton.tag = indexPath.row
            
            cell.didPressOnButton = { f in
                print("Closure Activated")
                self.viewModel.userPressed(at: indexPath)
//                self.goToChat(orderId: f)
            }
            
            let cellVM = viewModel.getCellViewModel(at: indexPath)
            cell.bidCellViewModel = cellVM
            
            return cell
        }
        return UITableViewCell()
    }
    
    
}

extension BidOffersViewController: backButtonViewDelegate {
    
    func didbackButtonPressed() {
        print("Delegate fired")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}

