//
//  OfferDetailsViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 2/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import FSPagerView

class OfferDetailsViewController: BaseViewController {
    
    lazy var viewModel: OfferDetailsViewModel = {
        return OfferDetailsViewModel()
    }()
    
    var error: APIError?
    
    var offerID: Int?
    var offerDetails: OfferDetails?
    var offerDetailsImages: [String]?
    
    var technicianDetails: TechnicianDetails?
    var technician: Technician?
    
    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View

    @IBOutlet weak var reportButton: UIButton!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(UINib(nibName: "cell", bundle: nil), forCellWithReuseIdentifier: "cell")
            self.pagerView.itemSize = FSPagerView.automaticSize
        }
    }
    @IBOutlet weak var pageControlView: FSPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        initVM()
        configureFSPageControl()
        print(offerID)
    }

    // MARK:- Init View Model
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
                        
        viewModel.reloadOfferDetails = { [weak self] () in
            DispatchQueue.main.async { [weak self] () in
                self?.offerDetails = self!.viewModel.offerDetails
                self?.tableView.reloadData()
            }
        }
                
        viewModel.updateImages = { [weak self] () in
            DispatchQueue.main.async { [weak self] () in
                self?.offerDetailsImages = self!.viewModel.offerDetailsImages
//                print(self?.offerDetailsImages)
//                self?.tableView.reloadData()
                if let images = self!.offerDetailsImages {
                    self!.pageControlView.numberOfPages = images.count
                    self?.pagerView.reloadData()
                    self!.pagerView.transformer = FSPagerViewTransformer(type: .cubic)
                    self!.pagerView.automaticSlidingInterval = 5.0
                }
    //                        self.pageControl.numberOfPages = 1
                self!.pageControlView.contentHorizontalAlignment = .right
                self!.pageControlView.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
            }
        }
        
        viewModel.updateSelectedOffer = { [weak self] () in
            DispatchQueue.main.async {
                self?.offerDetails = self!.viewModel.selectedOffer
                if let offer = self?.offerDetails {
                    print(offer)
                    self?.goToChat(orderId: offer.id!, workerID: offer.workerId!, workerName: offer.workerName!, price: offer.price!, technicianDetails: offer.technicianDetails!)
                }
            }
        }
                                            
        if let offerID: Int = self.offerID {
            viewModel.initOfferDetails(offerID: offerID)
        }
                
    }
        
    func configureFSPageControl() {
        pagerView.delegate = self
        pagerView.dataSource = self
        pagerView.reloadData()
    }
    
    func configureView() {
        self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
        reusableView.frame = self.navView.bounds
        reusableView.titleLabel.text = "offer details title".localized
        reusableView.titleImage.image = UIImage(named: "offer")
        reusableView.backButtonDelegate = self
        self.navView.addSubview(reusableView)
        chatButton.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        reportButton.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        self.pagerView.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        bottomView.roundCorners([.topLeft, .topRight], radius: 30.0)
        chatButton.setTitle("chat button".localized, for: .normal)
//        reportButton.setTitle("report".localized, for: .normal)
        reportButton.setTitle("report option".localized, for: .normal)
//        latestOffersView.roundCorners([.bottomLeft, .topLeft], radius: 8.0)
//        specialOrdersView.roundCorners([.bottomRight, .topRight], radius: 8.0)
    }

//MARK:- Before Block Edit
    // MARK:- Show Options Picker
//    func setupOptionSheet() {
//        let sheet = UIAlertController()
////        let sheet = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
//        sheet.addAction(UIAlertAction(title: "report option".localized, style: .default, handler: {_ in
//            self.showReportPopUp()
//        }))
//        sheet.addAction(UIAlertAction(title: "block option".localized, style: .default, handler: {_ in
//            self.showBlockPopUp()
//        }))
//        sheet.addAction(UIAlertAction(title: "report cancel button".localized, style: .cancel, handler: nil))
////        sheet.popoverPresentationController?.sourceView = s1WeigthLabel
//        self.present(sheet, animated: true, completion: nil)
//    }
//
//    func showReportPopUp() {
//        let alertController = UIAlertController(title: "report title popup".localized, message: "report message popup".localized, preferredStyle: .alert)
//        let openAction = UIAlertAction(title: "report okay button".localized, style: .default) { (action) in
////            self.goToHomePage()
//        }
//        let cancelAction = UIAlertAction(title: "report cancel button".localized, style: .cancel)
//        alertController.addAction(openAction)
//        alertController.addAction(cancelAction)
//        self.present(alertController, animated: true, completion: nil)
//    }
//
//    func showBlockPopUp() {
//        let alertController = UIAlertController(title: "block title popup".localized, message: "block message popup".localized, preferredStyle: .alert)
//        let openAction = UIAlertAction(title: "block okay button".localized, style: .default) { (action) in
////            self.goToHomePage()
//        }
//        let cancelAction = UIAlertAction(title: "block cancel button".localized, style: .cancel)
//        alertController.addAction(openAction)
//        alertController.addAction(cancelAction)
//        self.present(alertController, animated: true, completion: nil)
//    }
    
    func setupOptionSheet() {
        let sheet = UIAlertController()
//        let sheet = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        sheet.addAction(UIAlertAction(title: "report option".localized, style: .default, handler: {_ in
            self.showReportPopUp()
        }))
//        sheet.addAction(UIAlertAction(title: "block option".localized, style: .default, handler: {_ in
//            self.showBlockPopUp()
//        }))
        sheet.addAction(UIAlertAction(title: "report cancel button".localized, style: .cancel, handler: nil))
//        sheet.popoverPresentationController?.sourceView = s1WeigthLabel
        self.present(sheet, animated: true, completion: nil)
    }
    
    func showReportPopUp() {
        let alertController = UIAlertController(title: "report title popup".localized, message: "report message popup".localized, preferredStyle: .alert)
        let openAction = UIAlertAction(title: "report okay button".localized, style: .default) { (action) in
//            self.goToHomePage()
        }
        let cancelAction = UIAlertAction(title: "report cancel button".localized, style: .cancel)
        alertController.addAction(openAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
        
    func showBlockPopUp() {
        let alertController = UIAlertController(title: "block title popup".localized, message: "block message popup".localized, preferredStyle: .alert)
        let openAction = UIAlertAction(title: "block okay button".localized, style: .default) { (action) in
//            self.goToHomePage()
        }
        let cancelAction = UIAlertAction(title: "block cancel button".localized, style: .cancel)
        alertController.addAction(openAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }


        
    func configureTableView() {
        tableView.register(UINib(nibName: "OfferDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "OfferDetailsTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    // MARK:- Actions
    @IBAction func chatButtonIsPressed(_ sender: Any) {
        if let offerDetails: OfferDetails = self.offerDetails {
            self.viewModel.selectedOffer = offerDetails
        }
    }
    
    @IBAction func reportButtonIsPressed(_ sender: Any) {
        setupOptionSheet()
    }
    
    // MARK: - Navigation
    func goToChat(orderId: Int, workerID: Int, workerName: String, price: Int, technicianDetails: Technician) {
        print("RRRRRRRRRRRRRRRR")
        if let editProfileVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController {
            
            editProfileVC.orderId = orderId
            editProfileVC.technicianName = workerName
            editProfileVC.technicianID = workerID
            editProfileVC.bidPrice = price
            editProfileVC.isBid = false
            editProfileVC.technicianDetails = technicianDetails
            editProfileVC.technicianDetails?.name = workerName
            
            let navigationController = UINavigationController(rootViewController: editProfileVC)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        }
    }


}

extension OfferDetailsViewController: FSPagerViewDataSource,FSPagerViewDelegate {
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        if let numberOfItems: Int = self.offerDetailsImages?.count {
            return numberOfItems
        }
        return 0
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index) as! cell
        cell.viewModels = self.offerDetailsImages![index]
        return cell
    }
    
    // MARK:- FSPagerView Delegate
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControlView.currentPage = targetIndex
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.pageControlView.currentPage = pagerView.currentIndex
    }
    
    
    
//    @IBAction func sliderValueChanged(_ sender: UISlider) {
//        switch sender.tag {
//        case 1:
//            let newScale = 0.5+CGFloat(sender.value)*0.5 // [0.5 - 1.0]
//            self.pagerView.itemSize = self.pagerView.frame.size.applying(CGAffineTransform(scaleX: newScale, y: newScale))
//        case 2:
//            self.pagerView.interitemSpacing = CGFloat(sender.value) * 20 // [0 - 20]
//        case 3:
//            self.numberOfItems = Int(roundf(sender.value*7.0))
//            if let numberOfPages = self.numberOfItems {
//                self.pageControl.numberOfPages = numberOfPages
//            }
//            self.pagerView.reloadData()
//        default:
//            break
//        }
//    }

}


extension OfferDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.offerDetails != nil {
            return 4
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: OfferDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OfferDetailsTableViewCell") as? OfferDetailsTableViewCell {
            
            if indexPath.row == 0 {
                cell.offerDetailsLabel.text = self.offerDetails?.title
            } else if indexPath.row == 1 {
                cell.offerDetailsLabel.text = self.offerDetails?.description
            } else if indexPath.row == 2 {
                if let offerExpireDate: String = self.offerDetails?.expireDate {
                    if "Lang".localized == "en" {
                        cell.offerDetailsLabel.text = "End in: " + offerExpireDate
                    } else if "Lang".localized == "ar" {
                        cell.offerDetailsLabel.text = "ينتهي في: " + offerExpireDate
                    }
                }
            } else if indexPath.row == 3 {
                cell.footerView.isHidden = true
                if let offerPrice: Int = self.offerDetails?.price {
                    if "Lang".localized == "en" {
                        cell.offerDetailsLabel.text = "Price: " + String(offerPrice)
                    } else if "Lang".localized == "ar" {
                        cell.offerDetailsLabel.text = "السعر: " + String(offerPrice)
                    }
                }
            }

            return cell

        }
        
        return UITableViewCell()
    }
    
    
}

extension OfferDetailsViewController: backButtonViewDelegate {
    
    func didbackButtonPressed() {
        print("Delegate fired")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}
