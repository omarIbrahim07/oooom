//
//  RateNowViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/1/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import Cosmos

class RateNowViewController: BaseViewController {
    
    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
    
    lazy var viewModel: RateNowViewModel = {
        return RateNowViewModel()
    }()
    
    var rateArray: [Float] = [3,3,3,3,3]

    let englishRateArray: [String] = [
        "Price for service",
        "Cleaness",
        "Commitment",
        "Performance",
        "Personal rating",
    ]
    
    let arabicRateArray: [String] = [
        "السعر مقابل الخدمة",
        "النظافة",
        "الالتزام بالموعد",
        "أداء الخدمة",
        "التقييم الشخصي"
    ]
    
    var orderID: Int?
    var error: APIError?
    var status: Bool?

    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var rateTextView: UITextView!
    @IBOutlet weak var rateNowButton: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configure()
        configureTableView()
        
        initVM()
    }
    
    // MARK: - Configuration
    func configureView() {
        self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        bottomView.roundCorners([.topLeft, .topRight], radius: 30.0)
        let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
        reusableView.frame = self.navView.bounds
        reusableView.titleLabel.text = "maintenance orders".localized
        reusableView.titleImage.image = UIImage(named: "improvement")
        reusableView.backButtonDelegate = self
        self.navView.addSubview(reusableView)
        rateNowButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func configure() {
        rateTextView.delegate = self
        rateTextView.text = "rate text view placeholder".localized
        rateTextView.textColor = #colorLiteral(red: 0.2901960784, green: 0.6745098039, blue: 0.9137254902, alpha: 1)
        rateNowButton.setTitle("rate now button".localized, for: .normal)
        rateTextView.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        closeKeypad()
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "RateNowTableViewCell", bundle: nil), forCellReuseIdentifier: "RateNowTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        navView.addGestureRecognizer(tapGesture)
//        rateTextView.addGestureRecognizer(tapGesture)
//        view.addGestureRecognizer(tapGesture)
//        tapGesture.cancelsTouchesInView = true
//        tableView.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        rateTextView.endEditing(true)
    }
    
    // MARK:- Init View Model
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    self.stopLoadingWithSuccess()
                }
            }
        }
        
        viewModel.updateStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
               
                self.status = self.viewModel.getStatus()
                if let status = self.status {
                    if status == true {
                        self.showDonePopUp()
                    }
                }
            }
        }
    }
    
    func makeError(message: String) {
        let apiError = APIError()
        apiError.message = message
    }
    
    
    // MARK:- Actions
    @IBAction func rateNowButtonIsPressed(_ sender: Any) {
        guard let orderID: Int = self.orderID, orderID > 0 else {
            let apiError = APIError()
            apiError.message = "لا يوجد طلب"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let comment: String = self.rateTextView.text, comment.count >= 0 else {
            let apiError = APIError()
            apiError.message = "لا يوجد تعليق"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
                
        self.viewModel.rateOrder(orderID: orderID, rating: self.rateArray, comment: comment)
    }

    func showDonePopUp() {
        let alertController = UIAlertController(title: "تم تقييم هذا الطلب !", message: "الرجوع إلى الرئيسية", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "تم", style: .default) { (action) in
            self.goToHomePage()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
        goToHomePage()
    }

    // MARK: - Navigation
    func goToHomePage() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }

    func goToLogIn() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "LoginNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
}

extension RateNowViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rateArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: RateNowTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RateNowTableViewCell") as? RateNowTableViewCell {
            
            if "Lang".localized == "ar" {
                cell.ratingLabel.text = arabicRateArray[indexPath.row]
            } else if "Lang".localized == "en" {
                cell.ratingLabel.text = englishRateArray[indexPath.row]
            }
            cell.ratingView.tag = indexPath.row
            cell.delegate = self
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    
}

extension RateNowViewController: feedBackTableViewCellDelegate {
    func didfeedBackViewRated(tag: Int, rate: Float) {
        self.rateArray[tag] = rate
        print(self.rateArray)
    }
}

extension RateNowViewController: backButtonViewDelegate {
    
    func didbackButtonPressed() {
        print("Delegate fired")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}

extension RateNowViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == #colorLiteral(red: 0.2901960784, green: 0.6745098039, blue: 0.9137254902, alpha: 1) {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "rate text view placeholder".localized
            textView.textColor = #colorLiteral(red: 0.2901960784, green: 0.6745098039, blue: 0.9137254902, alpha: 1)
        }
    }

}
