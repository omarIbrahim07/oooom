//
//  RateNowTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 1/23/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import Cosmos

protocol feedBackTableViewCellDelegate {
    func didfeedBackViewRated(tag: Int, rate: Float)
}

class RateNowTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var ratingLabel: UILabel!
    
    var delegate: feedBackTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        ratingView.didTouchCosmos = { rating in
            self.rate(cosmosViewIndex: self.ratingView.tag )
        }

    }
    
    func didfeedBackViewRated(tag: Int, rate: Float) {
        if let delegateValue = delegate {
            delegateValue.didfeedBackViewRated(tag: tag, rate: rate)
        }
    }
    
    
    func rate(cosmosViewIndex: Int) {
        didfeedBackViewRated(tag: cosmosViewIndex, rate: Float(self.ratingView!.rating))
    }
    
}
