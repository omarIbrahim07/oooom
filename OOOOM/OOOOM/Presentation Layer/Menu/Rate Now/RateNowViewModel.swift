//
//  RateNowViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/8/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class RateNowViewModel {
    
    var error: APIError?

    var updateLoadingStatus: (()->())?
    var updateStatus: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
        
    // callback for interfaces
    var status: Bool? {
        didSet {
            self.updateStatus?()
        }
    }

    
    func rateOrder(orderID: Int, rating: [Float], comment: String) {
    state = .loading
    
    let params: [String : AnyObject] = [
        "order_id" : orderID as AnyObject,
        "comment" : comment as AnyObject,
        "price_rating" : rating[0] as AnyObject,
        "clean_rating" : rating[1] as AnyObject,
        "time_rating" : rating[2] as AnyObject,
        "performance_rating" : rating[3] as AnyObject,
        "rating" : rating[4] as AnyObject,
    ]
    
    OrderAPIManager().rateOrder(basicDictionary: params, onSuccess: { (status) in
        
        self.status = status
        if self.status == true {
            self.status = true
            self.state = .populated
        } else if self.status == false {
            self.state = .empty
            self.makeError(message: "لا يمكنك تقييم هذا الطلب مرتين")
        }


        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func makeError(message: String) {
        let apiError = APIError()
        apiError.message = message
        self.error = apiError
        self.state = .error
    }

    func getError() -> APIError {
        return self.error!
    }

    func getStatus() -> Bool {
        return self.status!
    }
}
