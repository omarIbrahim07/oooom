//
//  ListOfChatsViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/21/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import Firebase

class ListOfChatsViewController: BaseViewController {
    
    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
    
    var chatLists: [ChatList] = [ChatList]()
    var chatMessage: ChatMessage?
    var chatMessages: [ChatMessage] = [ChatMessage]()
    
    var online: [String] = [String]()

    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        
        getListOfChats()
    }

    func configureView() {
        self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
        reusableView.frame = self.navView.bounds
        reusableView.titleLabel.text = "list of chats".localized
        reusableView.titleImage.image = UIImage(named: "chat-bubble")
        reusableView.backButtonDelegate = self
        self.navView.addSubview(reusableView)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "ListOfChatsTableViewCell", bundle: nil), forCellReuseIdentifier: "ListOfChatsTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
//     MARK:- Observe last message -- New
    func observeMessages(roomID: String) {
        let dataBaseRef = Database.database().reference()
        dataBaseRef.child(roomID).queryLimited(toLast: 1).observe(.childAdded) { (snapShot) in
            print(snapShot)
            if let dataArray = snapShot.value as? [String : Any] {

                    guard let fromUserID = dataArray["from_user_id"] as? String, let messageText = dataArray["message"] as? String, let messageType = dataArray["message_type"] as? String, let toUserID = dataArray["to_user_id"] as? String, let timeStamp = dataArray["timestamp"] as? String else { return }
                    let message = ChatMessage(fromUSerID: fromUserID, toUSerID: toUserID, message: messageText, messageType: messageType,timeStamp: timeStamp)
                    self.chatMessages.append(message)
//                    self.tableView.reloadData()

            }
        }
    }

    
    // MARK:- Observe last message -- Old
//    func observeMessages(roomID: Int) {
//        let roomID: String = String(roomID)
//        let dataBaseRef = Database.database().reference()
//        dataBaseRef.child(roomID).queryLimited(toLast: 1).observe(.childAdded) { (snapShot) in
//            print(snapShot)
//            if let dataArray = snapShot.value as? [String : Any] {
//
//                    guard let fromUserID = dataArray["from_user_id"] as? String, let messageText = dataArray["message"] as? String, let messageType = dataArray["message_type"] as? String, let toUserID = dataArray["to_user_id"] as? String, let timeStamp = dataArray["timestamp"] as? String else { return }
//                    let message = ChatMessage(fromUSerID: fromUserID, toUSerID: toUserID, message: messageText, messageType: messageType,timeStamp: timeStamp)
//                    self.chatMessages.append(message)
////                    self.tableView.reloadData()
//
//            }
//        }
//    }
    
    func observeOnline(workerID: Int) {
        let workerID: String = String(workerID)
        let dataBaseRef = Database.database().reference()
        dataBaseRef.child("online").child(workerID).child("state").observe(.value) { (snapShot) in
            print(snapShot)
            if let dataArray = snapShot.value as? [String : Any] {

                guard let state = dataArray["state"] as? String else { return }
                self.online.append(state)
                self.tableView.reloadData()
            }
        }
    }

    
    // MARK:- Networking
    func getListOfChats() {
        
        startLoading()
        
        weak var weakSelf = self

        let parameters: [String : AnyObject] = [:]

        ChatAPIManager().getListOfChats(basicDictionary: parameters, onSuccess: { (listOfChats) in

            print("List Of Chats")
            self.chatLists = listOfChats
            for i in 0..<self.chatLists.count {
//                self.observeMessages(roomID: self.chatLists[i].id!) -- old
                self.observeMessages(roomID: self.chatLists[i].newID!)
                self.observeOnline(workerID: self.chatLists[i].toID!)
            }
            
        self.stopLoadingWithSuccess()
            
//            for i in 0..<self.chatLists.count {
//                self.observeOnline(workerID: self.chatLists[i].toID!)
//            }

        }) { (error) in
            weakSelf?.stopLoadingWithSuccess()
        }
    }
    
    // MARK:- Navigation
    func goToChat(chatList: ChatList) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        
        viewController.technicianID = chatList.toID
        viewController.technicianName = chatList.toUserName
        
        self.present(viewController, animated: true, completion: nil)
//        navigationController?.pushViewController(viewController, animated: true)
    }

}

extension ListOfChatsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatMessages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: ListOfChatsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ListOfChatsTableViewCell", for: indexPath) as? ListOfChatsTableViewCell {
            if let chatList: ChatList = self.chatLists[indexPath.row], let chatMessage: ChatMessage = self.chatMessages[indexPath.row] {
                cell.bindTableViewCell(chatList: chatList, chatMessage: chatMessage)
            }
            
            if let online: String = self.online[indexPath.row] {
                cell.checkOnline(onlineCheck: online)
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Hey")
        if let chatList: ChatList = self.chatLists[indexPath.row] {
            goToChat(chatList: chatList)
        }
    }
    
}

extension ListOfChatsViewController: backButtonViewDelegate {

    func didbackButtonPressed() {
        print("Delegate fired")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}
