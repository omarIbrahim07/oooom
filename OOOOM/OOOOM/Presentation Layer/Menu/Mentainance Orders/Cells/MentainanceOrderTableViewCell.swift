//
//  MentainanceOrderTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/21/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class MentainanceOrderTableViewCell: UITableViewCell {
    
    var mentainanceOrderCellViewModel: MentainanceOrderCellViewModel? {
        didSet {
            if "Lang".localized == "ar" {
                if let serviceName: String = mentainanceOrderCellViewModel?.service {
                    mentainanceOrderLabel.text = serviceName
                }
            } else if "Lang".localized == "en" {
                if let serviceNameEn: String = mentainanceOrderCellViewModel?.serviceEn {
                    mentainanceOrderLabel.text = serviceNameEn
                }
            }
            
            if let createdAt: String = mentainanceOrderCellViewModel?.createdAt {
                mentainanceOrderTimeLabel.text = createdAt
            }
            
            if let id: Int = mentainanceOrderCellViewModel?.id {
                orderDetailsButton.tag = id
            }
        }
    }
    
    var didPressOnButton: ( (Int) -> Void )?
    
    @IBOutlet weak var View: UIView!
    @IBOutlet weak var mentainanceOrderLabel: UILabel!
    @IBOutlet weak var mentainanceOrderTimeLabel: UILabel!
    @IBOutlet weak var mentainanceOrderDetailLabel: UILabel!
    @IBOutlet weak var mentainanceOrderImage: UIImageView!
    @IBOutlet weak var orderDetailsButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureCell()
        configure()
    }
    
    func configureCell() {
        View.layer.cornerRadius = 8.0
        mentainanceOrderImage.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func configure() {
        mentainanceOrderDetailLabel.text = "details button".localized
    }
    
    @IBAction func detailsButtonPressed(_ sender: Any) {
        print("Details is pressed")
        if let selectedButton = (sender as AnyObject).tag {
            print((sender as AnyObject).tag)
            //            switch selectedButton {
            //            case .plus:
            //                count += 1
            //            case .minus:
            //                count -= 1
            //            }
            didPressOnButton?(selectedButton)
        }

    }
    
    
}
