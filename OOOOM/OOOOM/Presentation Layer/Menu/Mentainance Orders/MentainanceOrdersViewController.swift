//
//  MentainanceOrdersViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/21/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class MentainanceOrdersViewController: BaseViewController {
    
//    var mentainanceOrderNumber: Int? = 10
    lazy var viewModel: MentainanceOrdersViewModel = {
        return MentainanceOrdersViewModel()
    }()

    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
    
    @IBOutlet weak var noMentainanceOrdersLabel: UILabel!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        
        initVM()
    }
    
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
//                    self.stopLoadingWithError(error: <#T##APIError#>)
                    self.stopLoadingWithSuccess()
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.initFetch()
        self.noMentainanceOrdersLabel.isHidden = true

            
//                if let service = choosedService {
//                    viewModel.initFetch(service: service, sorting: 1)
//                }
            

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.setNoTechnician = { [weak self] () in
           guard let self = self else {
               return
           }
           
           switch self.viewModel.techsExistance {
           
           case .notech:
               self.noMentainanceOrdersLabel.isHidden = false
           case .techs:
               self.noMentainanceOrdersLabel.isHidden = true
           }
        }

    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "MentainanceOrderTableViewCell", bundle: nil), forCellReuseIdentifier: "MentainanceOrderTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func configureView() {
        self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
        reusableView.frame = self.navView.bounds
        reusableView.titleLabel.text = "maintenance orders".localized
        reusableView.titleImage.image = UIImage(named: "improvement")
        reusableView.backButtonDelegate = self
        self.navView.addSubview(reusableView)
        noMentainanceOrdersLabel.text = "no mentainance orders".localized
    }
    
    func goToOrderDetails(orderId: Int) {
        if let editProfileVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "OrderDetailsViewController") as? OrderDetailsViewController {
            editProfileVC.orderId = orderId
            //                rootViewContoller.test = "test String"
            self.present(editProfileVC, animated: true, completion: nil)
            print("My Account")
        }
    }


}

extension MentainanceOrdersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return mentainanceOrderNumber ?? 0
        return viewModel.numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: MentainanceOrderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MentainanceOrderTableViewCell") as? MentainanceOrderTableViewCell {
//            cell.orderDetailsButton.tag = indexPath.row
            
            cell.didPressOnButton = { f in
                print("Closure Activated")
                self.goToOrderDetails(orderId: f)
            }
            
            let cellVM = viewModel.getCellViewModel(at: indexPath)
            cell.mentainanceOrderCellViewModel = cellVM
            
            return cell
        }
        return UITableViewCell()
    }
    
    
}

extension MentainanceOrdersViewController: backButtonViewDelegate {
    
    func didbackButtonPressed() {
        print("Delegate fired")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}
