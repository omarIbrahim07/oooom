//
//  View.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/21/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

protocol backButtonViewDelegate {
    func didbackButtonPressed()
}

class View: UIView {

    var backButtonDelegate: backButtonViewDelegate?

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleImage: UIImageView!
    
    @IBAction func backButtonPressed(_ sender: Any) {
        print("Back is pressed")
        didbackButtonPressed()
    }
    
    func didbackButtonPressed() {
        if let delegateValue = backButtonDelegate {
            delegateValue.didbackButtonPressed()
        }
    }

}
