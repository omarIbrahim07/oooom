//
//  MentainanceOrdersViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/18/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class MentainanceOrdersViewModel {

    private var mentainanceOrders: [MentainanceOrder] = [MentainanceOrder]()
    
    var selectedMentainanceOrder: MentainanceOrder?
        
    enum noTechs {
        case notech
        case techs
    }
    
    var mentainanceOrdersCellViewModels: [MentainanceOrderCellViewModel] = [MentainanceOrderCellViewModel]() {
           didSet {
               self.reloadTableViewClosure?()
           }
       }
    
    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var setNoTechnician: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
        
    var techsExistance: noTechs = .notech {
        didSet {
            self.setNoTechnician?()
        }
    }
    
    var numberOfCells: Int {
         return mentainanceOrdersCellViewModels.count
     }

    func initFetch() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        TechnicianAPIManager().getMentainanceOrders(basicDictionary: params, onSuccess: { (mentainanceOrders) in
            
            self.processFetchedPhoto(mentainanceOrders: mentainanceOrders)
            self.state = .populated

        }) { (error) in
            print(error)
        }

    }
    
    func processFetchedPhoto( mentainanceOrders: [MentainanceOrder] ) {
        self.mentainanceOrders = mentainanceOrders // Cache
        
        if self.mentainanceOrders.count == 0 {
            self.techsExistance = .notech
        } else {
            self.techsExistance = .techs
        }
        
        var vms = [MentainanceOrderCellViewModel]()
        for mentainanceOrder in mentainanceOrders {
            vms.append( createCellViewModel(mentainanceOrder: mentainanceOrder) )
        }
        self.mentainanceOrdersCellViewModels = vms
    }
    
    func createCellViewModel( mentainanceOrder: MentainanceOrder ) -> MentainanceOrderCellViewModel {
                    
        return MentainanceOrderCellViewModel(service: mentainanceOrder.service!, serviceEn: mentainanceOrder.serviceEn!, createdAt: mentainanceOrder.createdAt!, id: mentainanceOrder.id!)
    }


    func getCellViewModel( at indexPath: IndexPath ) -> MentainanceOrderCellViewModel {
        return mentainanceOrdersCellViewModels[indexPath.row]
    }

    func userPressed( at indexPath: IndexPath ) {
        let mentainanceOrder = self.mentainanceOrders[indexPath.row]

        self.selectedMentainanceOrder = mentainanceOrder
    }

}
